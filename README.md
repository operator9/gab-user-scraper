Gab User Scraper
================

**NOTE: In this release only the zipped base data incl. 2021-08-19 is provided. Code will be
released later.**

This will scrape users using Gab's official and public Mastodon API end-points.

The data contains no sensitive data.

This project is not associated or affiliated with Gab.

Note: Gab has announces changes to their backend platform which may result in this scraper may not
work as-is when that change is published.

We'll consider if we will update the scraper for their new platform when that time comes.

Features
--------

- Includes an already scraped snapshot
- Can scrape in segments allowing for distributed scraping
- Can merge incremental databases
- Can update existing data
- Can continue scraping from where it left off and stop at end of database
- Can interpolate `created_at` dates for deleted records that doesn't include dates
- Can detect gaps in the ID series
- Produces a SQLite database incl. indices and VIEWs

Note that accounts may have been deleted after this snapshot was made. Also numbers such as
followers, following and number statuses, status etc. may be out of sync. This data can however be
updated simply by running the scraper.

Installation
------------

Make sure to have Node.js v14 or newer installed. Then use git to clone this repository:

    $ git clone https://codeberg.org/operator9/gab-user-scraper

or use the SSH version.

CD into the root folder of the project, then run:

    $ npm i -g .

This will give you a global command `gabusers` that you can run from any location.

Unzip the included scraped database into the root of the project as
`users.sqlite`. You can now use the scraper.

Usage
-----

To create a database from scratch or to update existing data, simply run:

    $ gabusers

To continue adding to a database run:

    $ gabusers -c

To scrape a segment (for example ID 100 to 1000):

    $ gabusers -f 100 -t 1000

To merge an incremental database to main (records may already exist and will simply be updated with
data from the incremental database):

    $ gabusers -m /path/to/incrementaldb.sqlite

To interpolate missing dates for deleted records:

    $ gabusers --interpolate

To detect gaps in the ID series:

    $ gabusers --gaps

See `gabusers -h` for more details.

Use DB Browser for SQLite to run your queries and export data.

(c) 2021 operator9, swampcreature
